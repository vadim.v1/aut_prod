import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as chrome_options
from selenium.webdriver.firefox.options import Options as firefox_options


def get_chrome_options():
    options = chrome_options()
    options.add_argument("headless")  # headless or chrome
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920,1080")
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-setuid-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    return options


def get_firefox_options():
    options = firefox_options()
    options.headless = True
    options.add_argument("--width=1920")
    options.add_argument("--height=1080")
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-setuid-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    return options


@pytest.fixture(scope="function")
def driver(request):
    browser = request.config.getoption('--browser')
    if browser == 'chrome':
        options = get_chrome_options()
        remote_url = "http://selenium__standalone-chrome:4444/wd/hub"
        driver = webdriver.Remote(command_executor=remote_url, options=options)
        #driver = webdriver.Chrome(options=options)
    elif browser == 'firefox':
        options = get_firefox_options()
        remote_url = "http://selenium__standalone-firefox:4444/wd/hub"
        driver = webdriver.Remote(command_executor=remote_url, options=options)
        #driver = webdriver.Firefox(options=options)
    else:
        raise ValueError(f"Unsupported browser: {browser}")
    request.cls.driver = driver
    yield driver
    driver.quit()


def pytest_addoption(parser):
    parser.addoption('--browser', help='Which test browser?', default='chrome')
