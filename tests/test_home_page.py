from base.links import Links
from pages.home_page import HomePage
import pytest
from base.data_for_test import ExpectedData
from base.locators import HomePageLocators as HPL


class TestHomePage:

    @pytest.mark.parametrize("link,current,expected", [(Links.HOME_PAGES[0], HPL.CATEGORY_BANNERS_H3, ExpectedData.CATEGORY_BANNERS_H3[0]),
                                                       (Links.HOME_PAGES[1], HPL.CATEGORY_BANNERS_H3, ExpectedData.CATEGORY_BANNERS_H3[1]),
                                                       (Links.HOME_PAGES[2], HPL.CATEGORY_BANNERS_H3, ExpectedData.CATEGORY_BANNERS_H3[2]),
                                                       (Links.HOME_PAGES[3], HPL.CATEGORY_BANNERS_H3, ExpectedData.CATEGORY_BANNERS_H3[3])])
    def test_category_banners_h3(self, driver, link, current, expected):
        page = HomePage(driver, link)
        page.open()
        page.category_banners_h3_are_correct(current, expected)

    @pytest.mark.parametrize("link,current,expected", [(Links.HOME_PAGES[0], HPL.CATEGORY_BANNERS_DESCRIPTION, ExpectedData.CATEGORY_BANNERS_DESCRIPTIONS[0]),
                                                       (Links.HOME_PAGES[1], HPL.CATEGORY_BANNERS_DESCRIPTION, ExpectedData.CATEGORY_BANNERS_DESCRIPTIONS[1]),
                                                       (Links.HOME_PAGES[2], HPL.CATEGORY_BANNERS_DESCRIPTION, ExpectedData.CATEGORY_BANNERS_DESCRIPTIONS[2]),
                                                       (Links.HOME_PAGES[3], HPL.CATEGORY_BANNERS_DESCRIPTION, ExpectedData.CATEGORY_BANNERS_DESCRIPTIONS[3])])
    def test_category_banners_description(self, driver, link, current, expected):
        page = HomePage(driver, link)
        page.open()
        page.category_banners_description_are_correct(current, expected)

