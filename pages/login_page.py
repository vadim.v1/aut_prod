from base.selenium_base import SeleniumBase
from base.locators import LoginPageLocators


class LoginPage(SeleniumBase):

    def click_on_login_button(self):
        login_button = self.is_visible(*LoginPageLocators.SIGN_IN_BUTTON)
        login_button.click()

    def email_error_is_correct(self, current_data):
        email_error = self.is_visible(*LoginPageLocators.EMAIL_ERROR)
        assert email_error.text == current_data

    def pass_error_is_correct(self, current_data):
        pass_error = self.is_visible(*LoginPageLocators.PASS_ERROR)
        assert pass_error.text == current_data

    def put_text_email_field(self, test_data):
        email_field = self.is_visible(*LoginPageLocators.EMAIL_FIELD)
        email_field.send_keys(test_data)

    def put_text_pass_field(self, test_data):
        email_field = self.is_visible(*LoginPageLocators.PASS_FIELD)
        email_field.send_keys(test_data)

    def error_in_top_is_correct(self, current_data):
        error_in_top = self.is_visible(*LoginPageLocators.ERROR_IN_TOP)
        assert error_in_top.text == current_data

    def should_not_be_error_in_top(self):
        assert self.is_not_element_present(*LoginPageLocators.ERROR_IN_TOP)

