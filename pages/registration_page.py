from base.selenium_base import SeleniumBase
from base.locators import RegistrationPageLocators


class RegistrationPage(SeleniumBase):

    def click_on_create_an_account_button(self):
        login_button = self.is_visible(*RegistrationPageLocators.CREATE_AN_ACCOUNT_BUTTON)
        login_button.click()

    def first_name_error_is_correct(self, current_data):
        first_name_error = self.is_visible(*RegistrationPageLocators.FIRSTNAME_ERROR)
        assert first_name_error.text == current_data

    def last_name_error_is_correct(self, current_data):
        last_name_error = self.is_visible(*RegistrationPageLocators.LASTNAME_ERROR)
        assert last_name_error.text == current_data

    def email_error_is_correct(self, current_data):
        last_name_error = self.is_visible(*RegistrationPageLocators.LASTNAME_ERROR)
        assert last_name_error.text == current_data

    def pass_error_is_correct(self, current_data):
        last_name_error = self.is_visible(*RegistrationPageLocators.LASTNAME_ERROR)
        assert last_name_error.text == current_data

    def confirm_pass_error_is_correct(self, current_data):
        last_name_error = self.is_visible(*RegistrationPageLocators.LASTNAME_ERROR)
        assert last_name_error.text == current_data

    def put_text_first_name_field(self, test_data):
        email_field = self.is_visible(*RegistrationPageLocators.FIRSTNAME_FIELD)
        email_field.send_keys(test_data)

    def put_text_last_name_field(self, test_data):
        email_field = self.is_visible(*RegistrationPageLocators.LASTNAME_FIELD)
        email_field.send_keys(test_data)

    def put_text_email_field(self, test_data):
        email_field = self.is_visible(*RegistrationPageLocators.EMAIL_FIELD)
        email_field.send_keys(test_data)

    def put_text_pass_field(self, test_data):
        email_field = self.is_visible(*RegistrationPageLocators.PASS_FIELD)
        email_field.send_keys(test_data)

    def put_text_confirm_pass_field(self, test_data):
        email_field = self.is_visible(*RegistrationPageLocators.CONFIRM_PASS_FIELD)
        email_field.send_keys(test_data)

    def should_not_be_error_in_top(self):
        assert self.is_not_element_present(*RegistrationPageLocators.ERROR_IN_TOP)

    def error_in_top_is_visible(self):
        error_in_top = self.is_visible(*RegistrationPageLocators.ERROR_IN_TOP)
        return error_in_top
