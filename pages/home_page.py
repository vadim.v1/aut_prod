from base.data_for_test import ExpectedData
from base.selenium_base import SeleniumBase
from base.locators import HomePageLocators


class HomePage(SeleniumBase):

    def category_banners_h3_are_correct(self, current, expected):
        category_banners_h3 = self.are_visible(*current)
        for i in range(len(category_banners_h3)):
            assert category_banners_h3[i].text == expected[i]

    def category_banners_description_are_correct(self, current, expected):
        category_banners_description = self.are_visible(*current)
        for i in range(len(category_banners_description)):
            assert category_banners_description[i].text == expected[i]



