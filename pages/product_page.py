from base.selenium_base import SeleniumBase
from base.locators import ProductPageLocators


class ProductPage(SeleniumBase):

    def is_hreflag_present(self):
        hreflang_en = self.is_present(*ProductPageLocators.HREFLANG_EN)
        return hreflang_en

    def is_canonical_present(self):
        canonical = self.is_present(*ProductPageLocators.CANONICAL)
        return canonical

    def home_is_visible(self):
        return self.is_visible(*ProductPageLocators.HOME_BREADCRUMBS)
