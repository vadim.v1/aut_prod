class Links:
    PRODUCT_WITH_OPTION_ENG = ["https://weathertechshop.eu/en/sunshades/jeep/cherokee/2014/",
                               "https://weathertechshop.eu/en/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
                               "https://weathertechshop.eu/en/sunshades/bmw/3-series-(e36)/1999/",
                               "https://weathertechshop.eu/en/sunshades/bmw/4-series-(f32f33f36)/2020/"]
    PRODUCT_WITH_OPTION_NO_ENG = [
        "https://weathertechshop.eu/fr/sunshades/jeep/cherokee/2014/",
        "https://weathertechshop.eu/fr/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/fr/sunshades/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/fr/sunshades/bmw/4-series-(f32f33f36)/2020/",
        "https://weathertechshop.eu/de/sunshades/jeep/cherokee/2014/",
        "https://weathertechshop.eu/de/floorliner/tesla/model-x/2018/",
        "https://weathertechshop.eu/de/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/de/sunshades/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/de/sunshades/bmw/4-series-(f32f33f36)/2020/",
        "https://weathertechshop.eu/it/sunshades/jeep/cherokee/2014/",
        "https://weathertechshop.eu/it/floorliner/tesla/model-x/2018/",
        "https://weathertechshop.eu/it/cargo-trunk-liner/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/it/sunshades/bmw/3-series-(e36)/1999/",
        "https://weathertechshop.eu/it/sunshades/bmw/4-series-(f32f33f36)/2020/"
    ]
    LOGIN_URL = "https://weathertechshop.eu/en/customer/account/login/"
    REGISTRATION_PAGE = "https://weathertechshop.eu/en/customer/account/create/"
    MY_ACCOUNT_PAGE = "https://weathertechshop.eu/en/customer/account/"
    HOME_PAGE_EN = "https://weathertechshop.eu/en/"
    HOME_PAGES = [
        "https://weathertechshop.eu/en/",
        "https://weathertechshop.eu/fr/",
        "https://weathertechshop.eu/de/",
        "https://weathertechshop.eu/it/"]
    PRODUCTS_NO_OPTIONS = ["https://weathertechshop.eu/en/home-business-products/coasters/",
                           "https://weathertechshop.eu/en/seat-protector/land-rover--range-rover/discovery-series-ii/2005/",
                           "https://weathertechshop.eu/en/sunshades/mazda/cx-5/2016/",
                           "https://weathertechshop.eu/en/seat-protector/land-rover--range-rover/freelander/2006/",
                           "https://weathertechshop.eu/en/side-window-deflector/land-rover--range-rover/freelander--freelander-2--lr2/2015/"]
    PRODUCTS_NO_OPTIONS_IT = ["https://weathertechshop.eu/it/side-window-deflector/kia/soul-ev/2019/",
                              "https://weathertechshop.eu/it/sunshades/mazda/cx-5/2016/",
                              "https://weathertechshop.eu/it/seat-protector/land-rover--range-rover/freelander/2006/",
                              "https://weathertechshop.eu/it/side-window-deflector/land-rover--range-rover/freelander--freelander-2--lr2/2015/",
                              "https://weathertechshop.eu/it/altri-prodotti/gift-bags/"]
    CATEGORIES_IT = ["https://weathertechshop.eu/it/prodotti-per-casa-ufficio/",
                     "https://weathertechshop.eu/it/altri-prodotti/",
                     "https://weathertechshop.eu/it/accessori-cellulari-e-tablet/"]
