from selenium.webdriver.common.by import By


class OptionPageLocators:
    FIRST_OPTION = (By.CLASS_NAME, "product-option-text")
    CONTINUE_BUTTON = (By.CLASS_NAME, "btn.btn-default.continue-button")
    HREFLANG_EN = (By.CSS_SELECTOR, '[hreflang="en"]')
    CANONICAL = (By.CSS_SELECTOR, '[rel="canonical"]')
    FINAL_OPTIONS = (By.CSS_SELECTOR, 'div.vehicle-attrs div')


class LoginPageLocators:
    SIGN_IN_BUTTON = (By.CSS_SELECTOR, ".column.main  #send2")
    EMAIL_FIELD = (By.ID, "email")
    PASS_FIELD = (By.CSS_SELECTOR, ".column.main  #pass")
    EMAIL_ERROR = (By.ID, "email-error")
    PASS_ERROR = (By.ID, "pass-error")
    ERROR_IN_TOP = (By.CSS_SELECTOR, ".message-error.error.message")


class MyAccountLocators:
    ACCOUNT_NAV = (By.CSS_SELECTOR, ".block.account-nav")


class RegistrationPageLocators:
    CREATE_AN_ACCOUNT_BUTTON = (By.CSS_SELECTOR, ".action.submit.primary")
    FIRSTNAME_ERROR = (By.ID, "firstname-error")
    LASTNAME_ERROR = (By.ID, "lastname-error")
    EMAIL_ERROR = (By.ID, "email_address-error")
    PASS_ERROR = (By.ID, "password-error")
    CONFIRM_PASS_ERROR = (By.ID, "password-confirmation-error")
    FIRSTNAME_FIELD = (By.ID, "firstname")
    LASTNAME_FIELD = (By.ID, "lastname")
    EMAIL_FIELD = (By.ID, "email_address")
    PASS_FIELD = (By.ID, "password")
    CONFIRM_PASS_FIELD = (By.ID, "password-confirmation")
    ERROR_IN_TOP = (By.CSS_SELECTOR, ".message-error.error.message")


class HomePageLocators:
    CATEGORY_BANNERS_H3 = (By.CSS_SELECTOR, '.banner-text h3')
    CATEGORY_BANNERS_DESCRIPTION = (By.CSS_SELECTOR, '.banner-text p')


class ProductPageLocators:
    HREFLANG_EN = (By.CSS_SELECTOR, '[hreflang="en"]')
    CANONICAL = (By.CSS_SELECTOR, '[rel="canonical"]')
    HOME_BREADCRUMBS = (By.CSS_SELECTOR, ".item.home")


class CategoryPageLocators:
    HOME_BREADCRUMBS = (By.CSS_SELECTOR, ".item.home")
